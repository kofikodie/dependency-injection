package dev.kofikodie.dependecyinjection.controllers;

        import dev.kofikodie.dependecyinjection.services.GreetingServiceImpl;
        import org.junit.Before;
        import org.junit.Test;

        import static org.junit.Assert.assertEquals;

/**
 * @author kofikodieaddo on 2019-04-05
 */

public class SetterInjectedControllerTest {
    private SetterInjectedController setterInjectedController;

    @Before
    public void setUp() throws Exception {
        this.setterInjectedController = new SetterInjectedController();
        this.setterInjectedController.setGreetingService(new GreetingServiceImpl());
    }

    @Test
    public void testGreeting() throws Exception {
        assertEquals(GreetingServiceImpl.HELLO_GURUS, setterInjectedController.sayHello());
    }
}
