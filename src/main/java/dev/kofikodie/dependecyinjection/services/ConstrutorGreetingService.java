package dev.kofikodie.dependecyinjection.services;

import org.springframework.stereotype.Service;

/**
 * @author kofikodieaddo on 2019-04-05
 */
@Service
public class ConstrutorGreetingService implements GreetingService {
    @Override
    public String sayGreeting(){
        return "Hello from constructor";
    }
}
