package dev.kofikodie.dependecyinjection.services;

/**
 * @author kofikodieaddo on 2019-04-05
 */
public interface GreetingService {
    String sayGreeting();
}
