package dev.kofikodie.dependecyinjection.services;

import org.springframework.stereotype.Service;

/**
 * @author kofikodieaddo on 2019-04-05
 */
@Service
public class GreetingServiceImpl implements GreetingService {

    public static final String HELLO_GURUS = "Hello from greeting interface";

    @Override
    public String sayGreeting() {
        return HELLO_GURUS;
    }
}
