package dev.kofikodie.dependecyinjection.controllers;

import dev.kofikodie.dependecyinjection.services.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * @author kofikodieaddo on 2019-04-05
 */
@Controller
public class SetterInjectedController {
    @Autowired
    @Qualifier("getterGreetingService")
    private GreetingService greetingService;

    public String sayHello(){
        return greetingService.sayGreeting();
    }

    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }
}
