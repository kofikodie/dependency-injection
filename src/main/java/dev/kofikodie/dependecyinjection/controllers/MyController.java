package dev.kofikodie.dependecyinjection.controllers;

import dev.kofikodie.dependecyinjection.services.GreetingService;
import org.springframework.stereotype.Controller;

/**
 * @author kofikodieaddo on 2019-04-04
 */
@Controller
public class MyController {
private GreetingService greetingService;

    public MyController(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String greet(){
        //System.out.println("hello");

        return greetingService.sayGreeting();
    }
}
