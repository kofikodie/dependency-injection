package dev.kofikodie.dependecyinjection;

import dev.kofikodie.dependecyinjection.controllers.ConstructorInjectedController;
import dev.kofikodie.dependecyinjection.controllers.MyController;
import dev.kofikodie.dependecyinjection.controllers.PropertyInjectedController;
import dev.kofikodie.dependecyinjection.controllers.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DependecyInjectionApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(DependecyInjectionApplication.class, args);

        MyController controller = (MyController) ctx.getBean("myController");


        System.out.println(controller.greet());
        System.out.println(ctx.getBean(PropertyInjectedController.class).sayHello());
        System.out.println(ctx.getBean(ConstructorInjectedController.class).sayHello());
        System.out.println(ctx.getBean(SetterInjectedController.class).sayHello());

    }

}
